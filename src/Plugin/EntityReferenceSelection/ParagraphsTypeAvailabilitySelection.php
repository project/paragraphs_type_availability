<?php

namespace Drupal\paragraphs_type_availability\Plugin\EntityReferenceSelection;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\paragraphs\ParagraphsTypeInterface;
use Drupal\paragraphs\Plugin\EntityReferenceSelection\ParagraphSelection;

/**
 * Default plugin implementation of the Entity Reference Selection plugin.
 *
 * @EntityReferenceSelection(
 *   id = "paragraphs_type_availability",
 *   label = @Translation("Paragraphs type availability"),
 *   group = "paragraphs_type_availability",
 *   entity_types = {"paragraph"}
 * )
 */
class ParagraphsTypeAvailabilitySelection extends ParagraphSelection {

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'entity_type_id' => '',
      'bundle' => '',
      'field' => '',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    /**
     * @var \Drupal\field_ui\Form\FieldConfigEditForm $form_object
     */
    $form_object = $form_state->getFormObject();
    /**
     * @var \Drupal\field\FieldConfigInterface $field_config
     */
    $field_config = $form_object->getEntity();

    $form['entity_type_id'] = [
      '#type' => 'hidden',
      '#default_value' => $field_config->getFieldStorageDefinition()
        ->getTargetEntityTypeId(),
    ];
    $form['bundle'] = [
      '#type' => 'hidden',
      '#default_value' => $field_config->getTargetBundle(),
    ];
    $form['field'] = [
      '#type' => 'hidden',
      '#default_value' => $field_config->getFieldStorageDefinition()->getName(),
    ];
    $form['configure'] = [
      '#markup' => $this->t('You can configure which paragraph types can be added per paragraph type instead of here. <a href="@url">View the list of paragraph types</a>', [
        '@url' => Url::fromRoute('entity.paragraphs_type.collection')
          ->toString(),
      ]),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function getSortedAllowedTypes() {
    $sorted_allowed_types = parent::getSortedAllowedTypes();

    $available_in_fields_key = $this->configuration['entity_type_id'] . '.' . $this->configuration['bundle'] . '.' . $this->configuration['field'];
    $filtered_sorted_allowed_types = [];
    foreach ($sorted_allowed_types as $paragraphs_type_id => $paragraphs_type_data) {
      $paragraphs_type = \Drupal::entityTypeManager()
        ->getStorage('paragraphs_type')
        ->load($paragraphs_type_id);
      if (!$paragraphs_type instanceof ParagraphsTypeInterface) {
        continue;
      }
      $available_in_fields = $paragraphs_type->getThirdPartySetting('paragraphs_type_availability', 'available_in_fields', []);

      if (in_array($available_in_fields_key, $available_in_fields)) {
        $filtered_sorted_allowed_types[$paragraphs_type_id] = $paragraphs_type_data;
      }
    }

    $context = [
      'entity_type_id' => $this->configuration['entity_type_id'],
      'bundle' => $this->configuration['bundle'],
      'field' => $this->configuration['field'],
    ];

    $this->moduleHandler->alter('paragraphs_type_availability_allowed_types', $filtered_sorted_allowed_types, $context);

    // Sort by label.
    uasort($filtered_sorted_allowed_types, function (array $a, array $b) {
      if ($a['label'] == $b['label']) {
        return 0;
      }
      return ($a['label'] < $b['label']) ? -1 : 1;
    });

    return $filtered_sorted_allowed_types;
  }

}
