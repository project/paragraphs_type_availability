<?php

/**
 * @file
 * @addtogroup hooks
 * @{
 */

/**
 * Alter the list of allowed paragraph types.
 *
 * @param array $allowed_types
 *   The list of paragraph types.
 * @param array $context
 *   The context this is called from.
 */
function hook_paragraphs_type_availability_allowed_types_alter(array &$allowed_types, array $context) {
}

/**
 * @} End of "addtogroup hooks".
 */
